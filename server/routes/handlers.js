(function() {
    const request = require("request");
    const Boom = require("boom");
    const settings = require("../config/settings")();
    const SHA1 = require("crypto-js/hmac-sha1");
    const jsesc = require('jsesc');
    const bot = require("../bots");
    const JL = require('jsnlog').JL;
    const jsnlog_nodejs = require('jsnlog-nodejs').jsnlog_nodejs;
    const dbUtil = require("../utils/db");

    module.exports = function() {
        // Decalre private variables here

        // Return all public variables
        var returnObj = {
            home: homehandlerFunc,
            fb: {
                webhook: fbWebhookHandlerFunc,
                webhookPre: fbWebhookPreHandlerFunc
            },
            jsnlog: jsnlogHandlerFunc,
            api: {
                optionsList: optionsListHandler
            }
        }
        return returnObj

        //Function Declarations
        function homehandlerFunc(req, reply) {
            reply.file('public/index.html')
        }

        function fbWebhookPreHandlerFunc(req, reply) {
            if (req.method == "get") {
                console.log("FB Webhook Verify")
                if (req.query['hub.mode'] === 'subscribe' && req.query['hub.verify_token'] === settings.fb.webhook_token) {
                    return reply(req.query['hub.challenge']).code(200);
                }
                console.log("Invalid webhook token");
                return reply(Boom.unauthorized("Invalid webhook token"));
            }
            if (req.method == "post") {
                reply.continue();
                //TO BE DONE - sort out payload Authentication, specailly for Attachement payload.
                // var encryptedPayload = SHA1(jsesc(JSON.stringify(req.payload), {
                //   'lowercaseHex': true
                // }).toString(), settings.fb.page_secret).toString();
                //
                // if (req.headers['x-hub-signature'] && req.headers['x-hub-signature'].toString().split("=")[1] === encryptedPayload) {
                //   //return reply.continue();
                //   console.log("All GOOD")
                // } else {
                //   console.error("Invalid X hub Signature");
                //   console.log(encryptedPayload)
                //   console.log(req.headers['x-hub-signature'])
                //   console.log(jsesc(JSON.stringify(req.payload), {
                //     'lowercaseHex': true
                //   }).toString())
                // }
            }
        }

        function jsnlogHandlerFunc(req, reply) {
            jsnlog_nodejs(JL, req.payload);
            reply();
        }

        function fbWebhookHandlerFunc(req, reply) {
            var data = req.payload;
            //console.log(data)
            if (data.object === "page") {
                // Repy as soon as we recieve data from a page
                reply(); // Hapi bydefault replies with staus code 200, required by fb
                data.entry.forEach(function(entry) {
                    if (entry.messaging) {
                        var pageID = entry.id;
                        var timeOfEvent = entry.time;
                        entry.messaging.forEach(function(event) {
                            if (event.message || event.postback) {
                                //Handle Message Event can be a text message or a Quick reply
                                bot.fb.manage(event);
                            } else if (event.delivery) {
                                //Handle Delivery Event - Mainly used to analytics
                                console.log("Message Delivered");
                            } else if (event.read) {
                                //Handle Read event - Mainly used to analytics
                                console.log("Message Read");
                            } else {
                                // CHandle webview replies here.
                                console.log("Webhook receivd an Unknown Event", event);
                            }
                        })
                    }

                    if(entry.changes){
                        console.log(entry.changes)
                    }

                })
            } else if (data.object === "webview") {
                // Handle other FB post payloads - Webview
                if (data.event.sender.id) {
                    console.log("Handling post for object type Webview with payload : ", data.event)
                    data.event.reply = reply;
                    bot.fb.manage(data.event);
                } else {
                    console.log(data)
                    reply({ err: "No such user" })
                }
            } else {
                console.log()
            }
        }

        function optionsListHandler(req, reply) {
            const db = dbUtil.get();

            console.log(req.query)

            var occupations = db.collection("occupations")
            occupations.find({}, {
                _id: 0
            }).toArray(function(err, result) {
                if (!err) {
                    return reply({
                        err: null,
                        list: result
                    })
                }
                return reply({
                    err: "Unable to get Occupations List"
                })

            })
        }
    }();
})()