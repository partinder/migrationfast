(function() {
    'use strict';

    var fs = require('fs'),
        PDFParser = require("pdf2json");

    var pdfParser = new PDFParser();

    pdfParser.on("pdfParser_dataError", function(errData){
    	console.error(errData.parserError);
    });
    pdfParser.on("pdfParser_dataReady", function(pdfData) {

    	console.log(Object.keys(JSON.stringify(pdfData)));
    });

    pdfParser.loadPDF("./grant.pdf");

})();