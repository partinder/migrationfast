(function() {
    var settings = require("./settings")();
    var Boom = require("boom")

    function apiAuthFunc(request, reply) {
        if (request.headers['api-key'] && request.headers['api-key'] === settings.migrationfast_api_key) {
            return reply.continue({
                credentials: request.headers['api-key']
            })
        }

        if(request.method === "post" && request.headers['api-secret'] === settings.migrationfast_api_secret){
        		return reply.continue({
                credentials:  request.headers['api-secret']
            })
        }
        return reply(Boom.unauthorized("A valid API key is required"))
    }

    function apiSchemeFunc(server) {
        return {
            authenticate: apiAuthFunc
        }
    }
    exports = module.exports = {
        apiScheme: apiSchemeFunc
    }
}())