(function() {
    const request = require("request");
    const settings = require("../../config/settings")();
    const async = require("async")
    module.exports = function() {
        // Keep the send api call private
        var callSendAPI = callSendAPIFunc;
        var formatMessage = {
            text: formatTextMessageFunc,
            sender_action: formatSenderActionFunc,
            attachments: formatMultimediaMessageFunc,
            generic: formatGenericMessageFunc,
            quick_reply: formatQuickReplyFunc,
            button: formatButtonTemplateMessageFunc,
            list : formatListTemplateFunc
        }

        // Public Methods
        return sendMessageFunc;

        function sendMessageFunc(recipientID, messsages) {
            var messagesData = [];
            messsages.forEach(function(message) {
                messagesData.push(formatMessage[message.type](recipientID, message.payload))
            })
            callSendAPI(messagesData);
        }

        function callSendAPIFunc(messagesData) {
            // Run all messages in series / one after another
            var seriesFunc = []
                // push all post requests into the seriesFunc variable
            messagesData.forEach(function(message) {
                seriesFunc.push(
                    function(callback) {
                        var options = {
                            uri: 'https://graph.facebook.com/v2.6/me/messages',
                            qs: {
                                access_token: settings.fb.page_access_token
                            },
                            method: 'POST',
                            json: message
                        }
                        request(options, function(err, res, body) {
                            if (!err && res.statusCode == 200) {
                                var recipientID = body.recipient_id;
                                var messageID = body.message_id || message.sender_action;
                                //console.log("BOT REPLYING : Message with id %s Sent to user %d ", messageID, recipientID)
                                callback(null, messageID)
                            } else {
                                console.error("Unable to send message");
                                console.error(err);
                                console.log(res.body);
                                callback(err)
                            }
                        })
                    }
                )
            })

            // All post requests
            async.series(seriesFunc, function(err, results) {
                if (err) {
                    console.log(err)
                } else {
                    //console.log("All Messages sent to Client, refernce :", results)
                }
            })
        }

        function formatQuickReplyFunc(recipientID, message) {
            var messageData = {
                recipient: {
                    id: recipientID
                },
                message: {
                    text: message.text,
                    quick_replies: message.quick_replies
                }
            }
            return messageData;
        }

        function formatTextMessageFunc(recipientID, message) {
            var messageData = {
                recipient: {
                    id: recipientID
                },
                message: {
                    text: message
                }
            }
            return messageData;
        }

        function formatSenderActionFunc(recipientID, action) {
            var messageData = {
                recipient: {
                    id: recipientID
                },
                sender_action: action
            }

            return messageData
        }

        function formatMultimediaMessageFunc(recipientID, message) {

            var messageData = {
                    recipient: {
                        id: recipientID
                    },
                    message: {
                        attachment: {
                            type: message.type,
                            payload: {}
                        }
                    }
                }
                // url: message.url
                // can use attachment_id if we have the attachement id instead of url, for common media to all users.
            console.log("From manager :", message)
            if (message.payload.url) {
                messageData.message.attachment.payload.url = message.payload.url
            } else if (message.payload.attachment_id) {
                messageData.message.attachment.payload.attachment_id = message.payload.attachment_id
            }
            console.log("message to send: ", messageData)
            return messageData;
        }

        function formatGenericMessageFunc(recipientID, genericPayload) {
            var messageData = {
                recipient: {
                    id: recipientID
                },
                message: {
                    attachment: {
                        type: 'template',
                        payload: {
                            template_type: "generic",
                            elements: genericPayload
                        }
                    }
                }
            }

            return messageData
        }

        function formatButtonTemplateMessageFunc(recipientID, buttonTemplatePayload) {
            var messageData = {
                recipient: {
                    id: recipientID
                },
                message: {
                    attachment: {
                        type: 'template',
                        payload: {
                            template_type: "button",
                            text: buttonTemplatePayload.text,
                            buttons: buttonTemplatePayload.buttons
                        }
                    }
                }
            }

            return messageData
        }

        function formatListTemplateFunc(recipientID, listTemplatePayload) {
            var messageData = {
                recipient: {
                    id: recipientID
                },
                message: {
                    attachment: {
                        type: 'template',
                        payload: {
                            template_type: "button",
                            text: buttonTemplatePayload.text,
                            buttons: buttonTemplatePayload.buttons
                        }
                    }
                }
            }

            return messageData
        }
    }();
})()