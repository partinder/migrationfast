(function() {
    "use strict";
    const Hapi = require("hapi");
    const settings = require('./server/config/settings')();
    const routes = require("./server/routes/paths");
    const auth = require("./server/config/auth");
    const db = require("./server/utils/db");
    const setupData = require("./server/utils/setupData");

    const server = new Hapi.Server();
    server.connection({
        port: settings.port,
        host: settings.host
    });

    server.auth.scheme('api_scheme', auth.apiScheme);
    db.connect(function(err, db) {
        if (err)
            throw err;
        else {
            server.register([require("inert")], function(err) {
                if (err) {
                    throw err;
                }
                server.auth.strategy('api', "api_scheme");
                server.route(routes);
                server.start(function(err) {
                    if (err) {
                        throw err;
                    } else {
                        console.log("Server started a host : %s, port: %s", settings.host, settings.port);
                        setupData(function(err) {
                            if (err) throw err;
                            console.log("Data Setup");
                        });
                    }
                });
            });
        }
    });

})();