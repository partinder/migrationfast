(function() {
    const helpers = require('./helpers');
    const settings = require('../../config/settings')();
    const answers = require("./answers");
    const questionBank = require("./questionBank");



    var query_intro = [{
        questionID: "onshore",
        next: function(message, cb) {
            if (message.toLowerCase() === 'yes') {
                return cb(null, [{
                    questionID: ["visa_type"],
                    next: function userGender(message, cb) {
                        if (message.data === "2") {
                            return cb(null, [{ questionID: "user_gender" }]);
                        } else {
                            return cb();
                        }

                    }
                }]);
            } else {
                return cb(null, [{ questionID: "user_country" }]);
            }
        }
    }, {
        questionID: "user_email"
    }, {
        questionID: "user_phone"
    }, {
        questionID: "user_query"
    }, {
        process_answers: function(answers) {
            console.log("Answers : ", answers);
            return questionBank["query_thanks"]["question"];
        }
    }];

    var assessment_gsm = [{
        questionID: "occupation"
    }, {
        questionID: "user_age"
    }, {
        questionID: "maritial_status"
    }, {
        questionID: "skilled_employment_onshore",
        next: function(message, cb) {
            if (message.data.toLowerCase() === 'yes') {
                return cb(null, [{ questionID: 'employment_years' }]);
            } else {
                return cb();
            }
        }
    }, {
        questionID: "skilled_employment_offshore"
    }, {
        questionID: 'english',
        next: function(message, cb) {
            if (message.data === "yes") {
                return cb(null, [{
                    questionID: ["which_english"],
                    next: function(message, cb) {
                        return cb(null, [{
                            questionID: "english_score_reading"
                        }, {
                            questionID: "english_score_writing"
                        }, {
                            questionID: "english_score_speaking",
                        }, {
                            questionID: "english_score_listening"
                        }]);
                    }
                }]);
            }
            return cb();
        }
    }, {
        questionID: "qualification"
    }, {
        questionID: "studied_in_australia",
        next: function(message, cb) {
            if (message.data === "yes") {
                return cb(null, [{
                    questionID: "qualification_australia",
                    next: function(message, cb) {
                        if (message === "doctorate" || message === "master") {
                            return cb(null, [{
                                questionID: "specialist_education"
                            }]);
                        }
                        return cb();
                    }
                }]);
            }
            return cb();
        }
    }, {
        questionID: "gsm_others"
    }, {
        process_answers: function(ans) {

            return [
                answers.gsm_assessment(ans),
                helpers.typingOn(),
                questionBank['assessment_thanks']['question']
            ];
        }

    }];

    var my_occupation = [{
        questionID: "occupation_assessment",
        next: function(message, cb) {
            console.log(message.data);
            return cb(null, [{
                    process_answers: function(ans) {
                        return [
                            answers.occupation(ans)
                            // helpers.typingOn(),
                            // questionBank['assessment_thanks']['question']
                        ];
                    }
                },
                {
                    questionID: "occupation",
                    question: {
                        info: "Selected Occpuation from list offered",
                        question: {
                            type: "button",
                            payload: {
                                text: "Please seclect your occupation?",
                                buttons: message.data.map(function(item) {
                                    return {
                                        type: "postback",
                                        title: item.occupation,
                                        payload: item.anzsco
                                    };
                                })
                            }
                        },
                        answer_type: ["postback"],
                        validations: []
                    }
                }
            ]);
        }
    }];

    var my_visa_status = [{
        questionID: "user_age"
    },{
        questionID : "user_citizenship"
    }, {
        questionID: "user_passport"
    }, {
        questionID: "user_vgn"
    },{
        process_answers : function(ans){
            return [
                helpers.typingOn(),
                answers.my_visa_status(ans)
            ];
        }
    }];

    function QuestionManager() {
        this.questionBook = {
            'query_intro': query_intro,
            'assessment_gsm': assessment_gsm,
            "my_occupation": my_occupation,
            'my_visa_status' : my_visa_status
        };
        this.questionBank = questionBank;

        this.getQuestionBook = function(questionBookId) {
            return this.questionBook[questionBookId].slice(0);
        };
    }
    var questionManager = new QuestionManager();
    module.exports = questionManager;
})();

// question: {
//     type: 'generic',
//     payload: [{
//         "title": "Whats your nominated Occupation?",
//         // "image_url": "https://www.nwivisas.com/media/222132/Australian-Skilled-Occupation-List-2015-%E2%80%93-2016-published.png",
//         // "subtitle": "Skilled migrations are heavily dependent upon occupations in demand. Your prospects to migrate depend upon your current and/or previous occupations.",
//         "default_action": {
//             "type": "web_url",
//             "url": "https://3408db91.ngrok.io/bot/fb/occupations",
//             "webview_height_ratio": "full",
//             "messenger_extensions": "true"
//         },
//         "buttons": [{
//             "type": "web_url",
//             "url": "https://3408db91.ngrok.io/bot/fb/occupations",
//             "title": "Select Occupation",
//             "messenger_extensions": "true"
//         }]
//     }]
// },