(function() {
    // 'use strict';
    var cheerio = require("cheerio");
    var request = require("request");
    var fs = require("fs");
    var mongoClient = require("mongodb").MongoClient;
    var json2csv = require("json2csv");
    var async = require("async");

    Array.prototype.pushIf = function(ele) {
        if (this.indexOf(ele) == -1) {
            this.push(ele);
        }
    };

    var ANZSCO_page = "https://www.immigration.govt.nz/new-zealand-visas/apply-for-a-visa/tools-and-information/work-and-employment/full-occupation-list";

    var pages = ["ANZSCO","F2017L00850", "F2017L00848", "F2017L00851", "F2015L01018", "F2015L01148", "F2017L00834"];
    var seriesFunc = {};
    pages.forEach(function(id) {
        var link = id === "ANZSCO" ? ANZSCO_page : "http://www.legislation.gov.au/Details/" + id;
        seriesFunc[id] = function(cb) {
            console.log("Getting Link : ", link);
            request.get(link, function(err, res, body) {
                if (!err) {
                    console.log("Got Link : ", link);
                    fs.writeFileSync("./html/" + id + ".html", body);
                    return cb(null);
                }
                return cb(err);
            });
        };
    });

    async.series(seriesFunc, function(err, results) {
        if (!err) {
            console.log("All Files written, now parse HTML");
            parseHTML();
        } else {
            console.log("Something went wrong");
        }
    });
    
    function parseHTML() {

        var occupationsList = {};

        var parseFuncs = {
            ANZSCO: ANZSCO,
            F2017L00850: F2017L00850,
            F2017L00848: F2017L00848,
            F2017L00851: F2017L00851,
            F2015L01018: F2015L01018,
            F2015L01148: F2015L01148,
            F2017L00834: F2017L00834
        };

        pages.forEach(function(id) {
            parseFuncs[id]();
            console.log("Finished Parsing : ",id);
        });

        var insertObj = [];
        Object.keys(occupationsList).forEach(function(key) {
            insertObj.push({
                anzsco: key,
                occupation: occupationsList[key]["occupation"],
                list_type: occupationsList[key]["list_type"],
                visa: occupationsList[key]["visa"],
                skill_level: occupationsList[key]["skill_level"]
            });
        });

        //console.log(occupationsList);

        var fields = ["anzsco","occupation","list_type","visa","skill_level"];
        var result = json2csv({data: insertObj,fields : fields});
        fs.writeFile("skills.csv",result,function(err){
        	if(err)
        		throw err;
        	else
        		console.log("CSV file written");
        });

        mongoClient.connect('mongodb://localhost/db_myvisa', function(err, db) {
            if (!err) {
                // DB connected
                var skills_list = db.collection("skills_list");
                skills_list.insert(insertObj,{ordered : false, w: 0	}, function(err, result) {
                	if(!err){
                		console.log("Inserted");
                	} else {
                		console.log(err);
                	}
                });
            } else throw err;
        });

        function ANZSCO() {
            var $ = cheerio.load(fs.readFileSync("./html/"+arguments.callee.name.toString()+".html"));
            var tr = $("tbody tr");

            tr.each(function(i) {
                var td = $(tr[i]).find("td");
                occupationsList[$(td[1]).text().trim()] = {
                    occupation: $(td[0]).text().trim().toLowerCase(),
                    list_type: [],
                    visa: [],
                    skill_level: "0"
                };
            });
        }

        function F2017L00850() {

            var $ = cheerio.load(fs.readFileSync("./html/"+arguments.callee.name.toString()+".html"));

            var MTLSSL = $(".MsoNormalTable")[2];
            var STSOL = $(".MsoNormalTable")[3];

            var mTr = $(MTLSSL).find("tbody tr");
            var sTr = $(STSOL).find("tbody tr");


            mTr.each(function(i) {
                var td = $(mTr[i]).find("td");
                if (occupationsList[$(td[2]).text().trim()]) {
                    occupationsList[$(td[2]).text().trim()].list_type.pushIf("MTLSSL");
                    occupationsList[$(td[2]).text().trim()].visa = ["189", "489 - Family Nominated", "190", "489 - State or Territory nominated", "485"];
                }
            });

            sTr.each(function(i) {
                var td = $(sTr[i]).find("td");
                if (occupationsList[$(td[2]).text().trim()] && $(td[2]).text().trim() != "") {
                    occupationsList[$(td[2]).text().trim()].list_type.pushIf("STSOL");
                    occupationsList[$(td[2]).text().trim()].visa = ["190", "489 - State or Territory nominated"];
                }
            });
        }

        function F2017L00848() {
            var $ = cheerio.load(fs.readFileSync("./html/"+arguments.callee.name.toString()+".html"));

            var MTLSSL = $(".MsoNormalTable")[1];
            var STSOL = $(".MsoNormalTable")[2];

            var mTr = $(MTLSSL).find("tbody tr");
            var sTr = $(STSOL).find("tbody tr");

            mTr.each(function(i) {
                var td = $(mTr[i]).find("td");
                if (occupationsList[$(td[2]).text().trim()] && $(td[2]).text().trim() != "") {
                    occupationsList[$(td[2]).text().trim()].list_type.pushIf("MTLSSL");
                    occupationsList[$(td[2]).text().trim()].visa.pushIf("457");
                }
            });

            sTr.each(function(i) {
                var td = $(sTr[i]).find("td");
                if (occupationsList[$(td[2]).text().trim()] && $(td[2]).text().trim() != "") {
                    occupationsList[$(td[2]).text().trim()].list_type.pushIf("STSOL");
                    occupationsList[$(td[2]).text().trim()].visa.pushIf("457");
                }
            });
        }

        function F2017L00851() {
           var $ = cheerio.load(fs.readFileSync("./html/"+arguments.callee.name.toString()+".html"));

            var MTLSSL = $(".MsoNormalTable")[1];
            var STSOL = $(".MsoNormalTable")[2];

            var mTr = $(MTLSSL).find("tbody tr");
            var sTr = $(STSOL).find("tbody tr");


            mTr.each(function(i) {
                var td = $(mTr[i]).find("td");
                if (occupationsList[$(td[2]).text().trim()] && $(td[2]).text().trim() != "") {
                    occupationsList[$(td[2]).text().trim()].list_type.pushIf("MTLSSL");
                    occupationsList[$(td[2]).text().trim()].visa.pushIf("186");
                }
            });

            sTr.each(function(i) {
                var td = $(sTr[i]).find("td");
                if (occupationsList[$(td[2]).text().trim()] && $(td[2]).text().trim() != "") {
                    occupationsList[$(td[2]).text().trim()].list_type.pushIf("STSOL");
                    occupationsList[$(td[2]).text().trim()].visa.pushIf("186");
                }
            });
        }

        function F2015L01018() {
            var $ = cheerio.load(fs.readFileSync("./html/"+arguments.callee.name.toString()+".html"));

            var MTLSSL = $(".MsoNormalTable")[0];
            var STSOL = $(".MsoNormalTable")[2];

            var mTr = $(MTLSSL).find("tbody tr");
            var sTr = $(STSOL).find("tbody tr");


            mTr.each(function(i) {
                var td = $(mTr[i]).find("td");
                if (occupationsList[$(td[1]).text().trim().toString().toLowerCase()] && $(td[1]).text().trim() != "") {
                    occupationsList[$(td[1]).text().trim()].list_type.pushIf("RSMS");
                    occupationsList[$(td[1]).text().trim()].visa.pushIf("187");
                    occupationsList[$(td[1]).text().trim()].skill_level = $(td[2]).text().trim().toString();
                }
            });
        }

        function F2015L01148() {
            var $ = cheerio.load(fs.readFileSync("./html/"+arguments.callee.name.toString()+".html"));

            var MTLSSL = $(".MsoNormalTable")[0];
            var STSOL = $(".MsoNormalTable")[2];

            var mTr = $(MTLSSL).find("tbody tr");

            mTr.each(function(i) {
                var td = $(mTr[i]).find("td");
                if (occupationsList[$(td[1]).text().trim().toString().toLowerCase()] && $(td[1]).text().trim() != "") {
                    occupationsList[$(td[1]).text().trim()].list_type.pushIf("RSMS");
                    occupationsList[$(td[1]).text().trim()].visa.pushIf("187");

                }
            });
        }

        function F2017L00834() {
            var $ = cheerio.load(fs.readFileSync("./html/"+arguments.callee.name.toString()+".html"));

            var MTLSSL = $(".MsoNormalTable")[1];
            var STSOL = $(".MsoNormalTable")[2];

            var mTr = $(MTLSSL).find("tbody tr");
            var sTr = $(STSOL).find("tbody tr");


            mTr.each(function(i) {
                var td = $(mTr[i]).find("td");
                if (occupationsList[$(td[2]).text().trim()] && $(td[2]).text().trim() != "") {
                    occupationsList[$(td[2]).text().trim()].list_type.pushIf("MTLSSL");
                    occupationsList[$(td[2]).text().trim()].visa.pushIf("186");
                }
            });

            sTr.each(function(i) {
                var td = $(sTr[i]).find("td");
                if (occupationsList[$(td[2]).text().trim()] && $(td[2]).text().trim() != "") {
                    occupationsList[$(td[2]).text().trim()].list_type.pushIf("STSOL");
                    occupationsList[$(td[2]).text().trim()].visa.pushIf("186");
                }
            });
        }
    }
})();