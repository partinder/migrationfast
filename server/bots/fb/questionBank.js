(function() {
    var helpers = require("./helpers")
    var settings = require("../../config/settings")();
    exports = module.exports = {
        "onshore": {
            info: 'Onshore',
            question: {
                type: 'quick_reply',
                payload: {
                    text: 'Are you currently in Australia?',
                    quick_replies: [{
                        content_type: "text",
                        title: "Yes",
                        payload: "yes"
                    }, {
                        content_type: "text",
                        title: "No",
                        payload: "no"
                    }]
                }
            },
            answer_type: ['quick_reply', 'text'],
            validations: [helpers.validations.trueFalse]
        },
        'user_country': {
            info: "User Country",
            question: {
                type: "text",
                payload: "What country are you currently in?"
            },
            answer_type: ['text'],
            validations: []
        },
        'visa_type': {
            info: "Visa Type",
            question: {
                type: 'text',
                payload: "What visa are you on?"
            },
            answer_type: ['text'],
            validations: []
        },
        'user_email': {
            info: 'User Email',
            question: {
                type: 'text',
                payload: 'Whats your email id?'
            },
            answer_type: ['text'],
            validations: [helpers.validations.email]
        },
        'user_phone': {
            info: "User Phone",
            question: {
                type: 'text',
                payload: "Whats your phone number?"
            },
            answer_type: ["text"],
            validations: []
        },
        'user_query': {
            info: "User Question",
            question: {
                type: 'text',
                payload: "Your query?"
            },
            answer_type: ["text"],
            validations: []
        },
        "user_gender": {
            info: "User Gender",
            question: {
                type: "text",
                payload: "What's your gender?"
            },
            answer_type: ["text"],
            validations: []
        },
        'query_thanks': {
            info: "Query Thanks",
            question: {
                type: "text",
                payload: "Thank you for reaching out to us, One of our Expert Registered Migraiton Agent would assess your query and get back you soon."
            }
        },
        'occupation': {
            info: "User Occupation",
            question: {
                type: "button",
                payload: {
                    text: 'Whats your nominated Occupation?',
                    buttons: [{
                        type: "web_url",
                        url: settings.server_url + "/bot/fb/learn_more",
                        title: "Learn More",
                        webview_height_ratio: "tall"
                    }, {
                        type: "web_url",
                        url: settings.server_url + "/bot/fb/select/occupation",
                        messenger_extensions: "true",
                        title: "Select Occupation",
                        webview_height_ratio: "tall"
                    }]
                }
            },
            answer_type: ['webview'],
            validations: []
        },
        'user_age': {
            info: "User Age",
            question: {
                type: "text",
                payload: "What is your date of birth? (DD/MM/YYYY format)"
            },
            answer_type: ["text"],
            validations: [helpers.validations.dob]
        },
        'assessment_thanks': {
            info: "189 Assessment Thanks",
            question: {
                type: "text",
                payload: "One of our expert Registered Migration Agents will get back to your shortly to discuss your case."
            }
        },
        'skilled_employment_onshore': {
            info: "Skilled Employment Onshore",
            question: {
                type: "button",
                payload: {
                    text: "Have you been employed in your nominated occupation in Australia in the past 10 years?",
                    buttons: [{
                        type: "web_url",
                        title: "Learn More",
                        url: settings.server_url + '/bot/fb/learn_more'
                    }, {
                        type: "postback",
                        title: "Yes",
                        payload: "yes"
                    }, {
                        type: "postback",
                        title: "No",
                        payload: "no"
                    }]
                }
            },
            answer_type: ["postback", "text"],
            validations: [helpers.validations.trueFalse]
        },
        'employment_years': {
            info: "Number of years in employment",
            question: {
                type: 'text',
                payload: "How many years?"
            },
            answer_type: ["text"],
            validations: [helpers.validations.number]
        },
        "skilled_employment_offshore": {
            info: "Skilled employment offshore",
            question: {
                type: "button",
                payload: {
                    text: "Outside Australia, how many years have you been employed in your nominated occuaption in the past years?",
                    buttons: [{
                        type: "web_url",
                        url: settings.server_url + '/bot/fb/learn_more',
                        title: "Learn More",
                        webview_height_ratio: "tall"
                    }]
                }
            },
            answer_type: ["text"],
            validations: [helpers.validations.number]
        },
        "english": {
            info: "Taken test of English",
            question: {
                type: "button",
                payload: {
                    text: "Have you taken an exam to asses your English competency in the past 2 years",
                    buttons: [{
                        type: "web_url",
                        title: "Learn More",
                        url: settings.server_url + '/bot/fb/learn_more',
                        webview_height_ratio: "tall"
                    }, {
                        type: "postback",
                        title: "Yes",
                        payload: "yes"
                    }, {
                        type: "postback",
                        title: "No",
                        payload: "no"
                    }]
                }
            },
            answer_type: ["postback", "text"],
            validations: [helpers.validations.trueFalse]
        },
        "which_english": {
            info: "Which english exam : IELTS/PTE",
            question: {
                type: "button",
                payload: {
                    text: "Which exam have you given?",
                    buttons: [{
                        type: "postback",
                        title: "IELTS",
                        payload: "ielts",
                    }, {
                        type: "postback",
                        title: "PTE Academic",
                        payload: "pte"
                    }]
                }
            },
            answer_type: ['postback'],
            validations: []
        },
        "english_score_reading": {
            info: "Score in Reading module",
            question: {
                type: "text",
                payload: "Score in reading module?"
            },
            answer_type: ["text"],
            validations: [helpers.validations.number]
        },
        "english_score_writing": {
            info: "Score in Writing module",
            question: {
                type: "text",
                payload: "Score in writing module?"
            },
            answer_type: ["text"],
            validations: [helpers.validations.number]
        },
        "english_score_speaking": {
            info: "Score in Speaking module",
            question: {
                type: "text",
                payload: "Score in speaking module?"
            },
            answer_type: ["text"],
            validations: [helpers.validations.number]
        },
        "english_score_listening": {
            info: "Score in Listening module",
            question: {
                type: "text",
                payload: "Score in listening module?"
            },
            answer_type: ["text"],
            validations: [helpers.validations.number]
        },
        "studied_in_australia": {
            info: "Studied in Australia",
            question: {
                type: "quick_reply",
                payload: {
                    text: "Have you studied full time in Australia?",
                    quick_replies: [{
                        content_type: "text",
                        title: "Yes",
                        payload: "yes"
                    }, {
                        content_type: "text",
                        title: "No",
                        payload: "no"
                    }]
                }
            },
            answer_type: ["quick_reply", "text"],
            validations: [helpers.validations.trueFalse]
        },
        'qualification': {
            info: "Highest Qualification",
            question: {
                type: "quick_reply",
                payload: {
                    text: "Whats your highest qualification?",
                    quick_replies: [{
                        content_type: "text",
                        title: "Doctorate",
                        payload: "doctorate"
                    }, {
                        content_type: "text",
                        title: "Bachelor",
                        payload: "bachelor"
                    }, {
                        content_type: "text",
                        title: "Diploma / Trade",
                        payload: "diploma-trade"
                    }, {
                        content_type: "text",
                        title: "Others",
                        payload: "others"
                    }]
                }
            },
            answer_type: ["quick_reply"],
            validations: []
        },
        'qualification_australia': {
            info: "Highest Qualification",
            question: {
                type: "quick_reply",
                payload: {
                    text: "What qualification did you achieve in Australia?",
                    quick_replies: [{
                        content_type: "text",
                        title: "Doctorate",
                        payload: "doctorate"
                    }, {
                        content_type: "text",
                        title: "Masters",
                        payload: "master"
                    }, {
                        content_type: "text",
                        title: "Bachelor",
                        payload: "doctorate"
                    }, {
                        content_type: "text",
                        title: "Diploma/Trade",
                        payload: "diploma-trade"
                    }, {
                        content_type: "text",
                        title: "Other",
                        payload: "others"
                    }]
                }
            },
            answer_type: ["quick_reply"],
            validations: []
        },
        "specialist_education": {
            info: "Specialist Education in research",
            question: {
                type: "quick_reply",
                payload: {
                    text: "Was your a Masters degree by research or a Doctorate degree included at least two academic years in a relevant field.",
                    quick_replies: [{
                        content_type: "text",
                        title: "Yes",
                        payload: "yes"
                    }, {
                        content_type: "text",
                        title: "No",
                        payload: "no"
                    }]
                }
            },
            answer_type: ["quick_reply", "text"],
            validations: [helpers.validations.trueFalse]
        },
        "gsm_others": {
            info: "Others GSM assessment points",
            question: {
                type: "button",
                payload: {
                    text: "Do you meet any of these special criterias?",
                    buttons: [{
                        type: "web_url",
                        webview_height_ratio: "tall",
                        url: settings.server_url + '/bot/fb/select/189_others',
                        title: "Select",
                        messenger_extensions: true
                    }, {
                        type: "postback",
                        title: "No",
                        payload: "no"
                    }]
                },

            },
            answer_type: ["postback", "webview"],
            validations: []
        },
        "maritial_status": {
            type: "Maritual Status",
            question: {
                type: "quick_reply",
                payload: {
                    text: "Whats your Maritial Status?",
                    quick_replies: [{
                        content_type: "text",
                        title: "Never Married",
                        payload: 'never-married'
                    }, {
                        content_type: "text",
                        title: "Married",
                        payload: "married"
                    }, {
                        content_type: "text",
                        title: "Seperated",
                        payload: 'seperated'
                    }, {
                        content_type: "text",
                        title: "Divorced",
                        payload: "divorced"
                    }]
                }

            },
            answer_type: ['quick_reply'],
            validations: []
        },
        "get_started": {
            info: "Get Started",
            question: {
                type: "button",
                payload: {
                    text: "How can we help you today?",
                    buttons: [{
                        type: "postback",
                        title: "Ask a Question",
                        payload: "query_intro"
                    }, {
                        type: "postback",
                        title: "GSM Assessment",
                        payload: "assessment_gsm"
                    }]
                }
            }
        },
        "get_started_occupation": {
            info: "Get Started",
            question: {
                type: "button",
                payload: {
                    text: "Would you like to assess your visa options for your current occupation?",
                    buttons: [{
                        type: "postback",
                        title: "Yes",
                        payload: "my_occupation"
                    }]
                }
            }
        },
        "occupation_assessment": {
            info: "User Occupation",
            question: {
                type: "text",
                payload: "What is your current occupation?"
            },
            answer_type: ["text", "postback"],
            validations: [helpers.validations.occupation]
        },
        "user_passport": {
            info: "User Passport Number",
            question: {
                type: "text",
                payload: "What is your passport number?"
            },
            answer_type: ["text"],
            validations: [helpers.validations.passport]
        },
        "user_vgn": {
            info: "User Visa Grant Number",
            question: {
                type: "text",
                payload: "What is your 13 digit Visa Grant Number?"
            },
            answer_type: ["text"],
            validations: [helpers.validations.visaGrantNumber]
        },
        "user_citizenship": {
            info: "User Passport Country",
            question: {
                type: "text",
                payload: "What is your country of Citizenship?"
            },
            answer_type: ["text"],
            validations: [helpers.validations.citizenship]
        }

    };
}());