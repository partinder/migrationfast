(function() {
    var mongoClient = require("mongodb").MongoClient;
    var Fuse = require("fuse.js");

    var options = {
        shouldSort: true,
        includeScore: true,
        threshold: 0.3,
        location: 0,
        distance: 100,
        maxPatternLength: 32,
        minMatchCharLength: 4,
        keys: [
            "occupation"
        ]
    };


    mongoClient.connect("mongodb://localhost/db_myvisa", function(err, db) {
        if (err) throw err;
        db.collection("skills_list").find({}, { _id: 0 }).toArray(function(err, res) {
            if (err) throw err;
            var fuse = new Fuse(res, options);
            var result = fuse.search("cook");
            console.log(result);
        });

    });
}());