function routes($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('');
    $locationProvider.html5Mode(true);
    $routeProvider
        .when("/", {
            templateUrl: "/public/app/templates/webHome.html",
            controller: "webHomeController"
        })
        .when("/bot/fb/select/:selectionType", {
            templateUrl: function(params) {
                return "/public/app/templates/bot/selection.html"
            },
            controller: "selectionController",
            resolve: {
                MessengerExtensions: MessengerExtensionsFunc,
                optionsList: optionsList
            }
        })
        .when("/bot/fb/learn_more",{
            templateUrl : "/public/app/templates/bot/learn_more.html"
        })
        .when("/404", {
            templateUrl: "public/app/templates/404.html"
        })
        .otherwise({
            redirectTo: "/404"
        })
}

routes.$inject = ["$routeProvider", "$locationProvider"]

function MessengerExtensionsFunc($timeout) {
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.com/en_US/messenger.Extensions.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'Messenger'));

    // return "FB Extensiosn"
    var promise = new Promise(function(resolve, reject) {
        function checkFB() {
            if (typeof MessengerExtensions == 'undefined') {
                $timeout(function() {
                    checkFB()
                }, 1000)
            } else {
                resolve(MessengerExtensions)
            }
        }
        checkFB()
    })
    return promise
}
MessengerExtensionsFunc.$inject = ['$timeout']

function optionsList(botDataService, $route) {
    console.log($route.current.params.selectionType)
    return botDataService.getOptionsList($route.current.params.selectionType)
}
optionsList.$inject = ['botDataService', '$route']

function headerConfig(a) {
    a.defaults.headers.get = { "api-key": "z4nHKNrXid2BYxaezjohh40lyGxwWZHDg47Zbdge" }
    a.defaults.headers.post = { "api-secret": "hPzHprj5tgEs1stCpp98ky1hyxXz8OiZ4vCpXTRD", 'Content-Type': 'application/json' }
}
headerConfig.$inject = ["$httpProvider"]