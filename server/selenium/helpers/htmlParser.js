(function() {
    var cheerio = require("cheerio");
    var fs = require("fs");
    var mongoClient = require("mongodb").MongoClient;

    var htmlParser = {
        resultParser: resultParser,
        refIDParser: refIDParser,
        IDParser: IDParser,
        testIds: testIds
    };
    // Parse HTML
    function resultParser(html) {
        var $ = cheerio.load(html);

        var error = $(".wc-messagebox-type-error");
        var success = $(".wc-messagebox-type-info");
        var validationError = $(".wc-validationerrors");
        var errorString;
        var response = {};

        if (validationError.length > 0) {
            errorString = $(error).find(".wc-error").text().trim().replace(/(\r\n|\n|\r)/gm, "");
            if (errorString.indexOf("Visa Grant Number must be completed.") != -1) {
                response.error = "Selenium";
                response.data = "VGN field didn't load";
                //console.log("VALIDATTION ERROR : Visa Grant Input did not load, try again");
            } else {
                response.error = "Validation";
                response.data = "Ref Number";
            }
        } else if (error.length > 0) {
            response.error = "Data Error";
            errorString = $(error).find(".wc-message").text().trim().replace(/(\r\n|\n|\r)/gm, "");
            if (errorString.indexOf("The details entered could not be found.") != -1) {
                console.log("Some Data entered is wrong: Either DOB or Passport");
                response.data = "User Data Wrong";
            } else if (errorString.indexOf("You do not have a current Australian visa.") != -1) {
                response.data = "Visa Expired";
            } else if (errorString.indexOf("Entered details cannot be confirmed as the TRN/Visa Grant number/Evidence number does not match your current visa.") != -1) {
                response.data = "TRN/VGN number is wrong";
            }
        } else if (success.length > 0) {
            if ($(success).find(".wc-message").text().trim().replace(/(\r\n|\n|\r)/gm, "").indexOf("The entitlements associated with your current 'in-effect' visa are displayed below.") != -1) {
                // extract Visa Details
                var data = {};
                var fieldDiv = $(".wc-field");
                fieldDiv.each(function() {
                    if ($(this).children().first().text() === 'Visa condition(s)') {
                        data[$(this).children().first().text()] = [];
                        var that = this;
                        var conditions = $(this).children().last().find(".condition");
                        conditions.each(function() {
                            data[$(that).children().first().text().trim()].push($(this).text().split("-")[0].trim());
                        });
                    } else {
                        data[$(this).children().first().text()] = $(this).children().last().text();
                    }

                });
                response.error = null;
                response.data = data;
            }
        } else {
            response.error = "Other";
            response.data = html.toString();
        }
        return response;
    }

    function refIDParser(html) {
        var $ = cheerio.load(html);
        var input = $('input');
        var refID = "#" + $(input[2]).attr('id');
        return refID;
    }

    function IDParser(html) {

        const $ = cheerio.load(html);

        var select = $('select');
        var input = $('input');
        var button = $('button');



        var countries = $(select[1]).find('option');
        var countryID = "#" + $(select[1]).attr('id');
        var countryData = {};

        countries.each(function() {
            countryData[$(this).text().toLowerCase().toString()] = $(this).attr("value");
        });
        saveCountriestoDB(countryData);
        
        var ids = {
            dateOfBirthID: "#" + $(input[2]).attr('id'),
            passportID: "#" + $(input[3]).attr('id'),
            checkboxID: "#" + $(input[4]).attr('id'),
            submitID: "#" + $(button[6]).attr('id'),
            refTypeID: "#" + $(select[0]).attr('id'),
            countryID: Object.keys(countryData).length > 2 ? countryID : "#undefined"
        };

        var returnData = {};

        Object.keys(ids).forEach(function(id) {
            if (ids[id] != '#undefined') {
                console.log(ids[id]);
                returnData[id] = ids[id];
            }
        });
        return returnData;

    }

    function testIds(ids) {
        Object.keys(ids).forEach(function(id) {
            if (ids[id] == "")
                return "err";
        });
        return;
    }

    function saveCountriestoDB(countries){
        delete countries.country;
       

        var countriesArray = Object.keys(countries).map(function(countryName){
            return {
                country : countryName,
                country_code : countries[countryName]
            };
        });

        console.log(countriesArray);

        mongoClient.connect("mongodb://localhost/db_myvisa",function(err,db){
            if(err) throw err;

            db.collection("countries").insert(countriesArray,{ordered :false},function(err,result){
                if(err) throw err;
                console.log(result.insertedCount);
            });
        });

    }

    module.exports = htmlParser;
})();