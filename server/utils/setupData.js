(function() {
    var data = require("../config/data");
    exports = module.exports = function(cb) {
        var db = require("./db").get();
        db.collection("skills_list").find({}, { _id: 0 }).toArray(function(err, res) {
            if (err)
                console.error(err);
            else {
                data.skills_list = res;
                db.collection("countries").find({}, { _id: 0 }).toArray(function(err, res){
                    if (err) throw err;
                    var countriesObj = {};
                    res.forEach(function(item){
                        countriesObj[item.country] = item.country_code;
                    });
                    data.countries = countriesObj;
                    return cb();
                });
            }
        });
    };


}());