(function() {
    const settings = require("../../config/settings")();
    const request = require('request');
    const Fuse = require("fuse.js");

    var exportsFunc =

        module.exports = function() {
            var returnObj = {
                getUserProfile: getUserProfileFunc,
                validations: {
                    email: emailValidationFunc,
                    phone: phoneValidationFunc,
                    trueFalse: trueFalseValidationFunc,
                    number: numberValidationFunc,
                    dob: dobValidationFunc,
                    occupation: occupationValidationFunc,
                    passport: passportValidationFunc,
                    visaGrantNumber: visaGrantNumberValidationFunc,
                    citizenship: citizenshipValidationFunc
                },
                typingOn: typingOnFunc,
                errorMessage: errorMessageFunc,
                wrongAnswer: wrongAnswerFunc,
                textMessage: textMessageFunc,
                formatMessage: formatMessageFunc
            };
            return returnObj;
            // Function Declarations

            function formatMessageFunc(event) {

                var senderID = event.sender.id;
                // var recipientID = event.recipient.id || null;
                // var timeOfMessage = event.timestamp || null;
                // var message = formatMessage(event);

                var returnMessage;
                if (event.message) {
                    if (event.message.text) {
                        returnMessage = {
                            type: "text",
                            payload: event.message.text
                        };
                    }
                    if (event.message.quick_reply) {
                        returnMessage = {
                            type: 'quick_reply',
                            payload: event.message.quick_reply.payload
                        };
                    }
                    if (event.message.attachments) {
                        returnMessage = {
                            type: 'attachments',
                            payload: event.message.attachments
                        };
                    }
                }
                if (event.postback) {
                    returnMessage = {
                        type: "postback",
                        payload: event.postback.payload
                    };
                }
                if (event.webview) {
                    returnMessage = {
                        type: "webview",
                        payload: event.webview.payload,
                        reply: event.reply
                    };
                }

                return returnMessage;
            }

            function textMessageFunc(message) {
                return {
                    type: "text",
                    payload: message
                };
            }

            function typingOnFunc() {
                return {
                    type: 'sender_action',
                    payload: 'typing_on'
                };
            }

            function errorMessageFunc() {
                return {
                    type: 'text',
                    payload: "Something is not right, we are working on it."
                };
            }

            function wrongAnswerFunc() {
                return {
                    type: 'text',
                    payload: "Sorry, I can't understand your answer, Please try again :"
                };
            }

            function getUserProfileFunc(userId, cb) {
                var options = {
                    method: 'GET',
                    uri: 'https://graph.facebook.com/v2.6/' + userId,
                    qs: {
                        fields: 'first_name,last_name,profile_pic,locale,timezone,gender',
                        access_token: settings.fb.page_access_token
                    }
                };

                request(options, function(err, res, body) {
                    body = JSON.parse(body)
                    if (!err && res.statusCode === 200 && !body.error) {
                        cb(null, res.body);
                    } else {
                        console.log("Something is wrong")
                        console.log(body)
                        return cb();
                    }
                });
            }

            function emailValidationFunc(message) {

                if (validateEmail(message.data)) {
                    return makeReplyObj("data",message.data);
                }
                return null;

                function validateEmail(email) {
                    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    return re.test(email);
                }
            }

            function phoneValidationFunc(message) {

            }

            function trueFalseValidationFunc(message) {
                console.log("Do validation for : ", message.data);
                if (message.data === "yes" || message.data === "no") {
                    console.log("YES");
                    return makeReplyObj("data",message.data);
                }
                return null;
            }

            function numberValidationFunc(message) {
                var age = Number(message.data);
                console.log("user age is :", age);

                if (!isNaN(age)) {
                    return makeReplyObj("data",message.data);
                }
                console.log("returning null");
                return null;
            }

            function dobValidationFunc(message) {
                var validatedMessage = isValidDate(message.data);
                if (validatedMessage) {
                    return makeReplyObj("data",validatedMessage);
                }
                return makeReplyObj("err","Sorry, that doesnt seem to be a valid date of birth, please try again.");

                function isValidDate(dateString) {
                    // First check for the pattern
                    if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
                        return false;

                    // Parse the date parts to integers
                    var parts = dateString.split("/");
                    var day = parseInt(parts[0], 10);
                    var month = parseInt(parts[1], 10);
                    var year = parseInt(parts[2], 10);

                    // Check the ranges of month and year
                    if (year < 1000 || year > 3000 || month == 0 || month > 12)
                        return false;

                    var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
                    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                    // Adjust for leap years
                    if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
                        monthLength[1] = 29;

                    // Check the range of the day
                    if (day > 0 && day <= monthLength[month - 1]) {

                        return [day.toString().length == 1 ? 0 + day.toString() : day.toString(), monthNames[month - 1], year].join(" ");

                    }
                    return false;
                }
            }

            function occupationValidationFunc(message) {
                message = message.data.toString().toLowerCase();
                console.log("Occupation to search in Skills list  : ", message);
                var skills_list = require("../../config/data").skills_list;
                var options = {
                    shouldSort: true,
                    includeScore: true,
                    threshold: 0.2,
                    location: 0,
                    distance: 100,
                    maxPatternLength: 32,
                    minMatchCharLength: 3,
                    keys: [
                        "occupation",
                        "anzsco"
                    ]
                };
                var fuse = new Fuse(skills_list, options);
                var results = fuse.search(message);
                var returnObj = {};

                if (results.length > 0) {
                    // Some possible match
                    console.log(results);
                    results = results.length > 5 ? results.splice(0, 3) : results;
                    results = results.filter(result => result.score < 0.3);
                    results = results.map((result) => {
                        return {
                            occupation: capitalize(result.item.occupation),
                            anzsco: result.item.anzsco
                        };
                    });

                    results = results.length > 3 ? results.splice(0, 3) : results;
                    if (results.length === 0) {
                        returnObj = makeReplyObj("err","I can't understand that occupation, can you please try again?");
                        return returnObj;
                    }
                    return makeReplyObj("data",results);

                } else {
                    // No Match , text enterted by user doesnt match any occupation (FUZZY)
                    console.log("err","Message for which we cant find any match : ", message);
                    returnObj = makeReplyObj("I can't understand that occupation, can you please try again?");

                }
                return returnObj;
            }

            function capitalize(strings) {
                strings = strings.split(" ");
                strings = strings.map(string => string.charAt(0).toUpperCase() + string.slice(1));
                return strings.join(" ");
            }

            function passportValidationFunc(message) {

                if(message.data.length > 7){
                    return makeReplyObj("data",message.data.toUpperCase());
                }
                return makeReplyObj("err","Sorry, your Passport number seems to be invalid, please try again");
            }

            function citizenshipValidationFunc(message) {
                var countries = require("../../config/data").countries;
                message = message.data.toLowerCase();
                // if(countries)
                if(countries[message]){
                    return makeReplyObj("data",{country : message, country_code : countries[message]});
                }
                return makeReplyObj("err","Sorry, I can't recognize your country of Citizenship, please try again");

            }

            function visaGrantNumberValidationFunc(message) {
                if(message.data.length == 13){
                    return makeReplyObj("data",message.data.toUpperCase());
                }
                return makeReplyObj("err","Sorry, your Visa Grant Number seems to be invalid, please try again");
            }

            function makeReplyObj(type,msg){
                var returnObj = {};
                returnObj[type] = msg;
                return returnObj;
            }
        }();
})();