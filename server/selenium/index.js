(function() {
    'use strict';
    var webdriverio = require('webdriverio');
    var cheerio = require("cheerio");
    var options = {
        desiredCapabilities: {
            browserName: 'chrome'
        }
    };
    var settings = require("./helpers/settings");
    var htmlParser = require("./helpers/htmlParser");


     exports = module.exports = function myVevo(refType, refNumber, DOB, passportNo, country, cb) {
        var startTime = new Date();
        
        var refTypeID;
        var refID;
        var countryID;
        var dateOfBirthID;
        var passportID;
        var checkboxID;
        var submitID;
        var responseData;

        webdriverio
            .remote(options)
            .init()
            .url('https://online.immi.gov.au/evo/firstParty?actionType=query')
            .pause(25000)
            .getHTML("/html/body")
            .then(function(html) {
                
                var ids = htmlParser.IDParser(html);
                dateOfBirthID = ids.dateOfBirthID;
                passportID = ids.passportID;
                countryID = ids.countryID;
                checkboxID = ids.checkboxID;
                submitID = ids.submitID;
                refTypeID = ids.refTypeID;

                console.log(Object.keys(ids).length);
                //console.log("Keys Length : ", Object.keys(ids.countryData).length);
                if(Object.keys(ids).length != 6) {
                    this.end();
                    return cb({
                        error: "Selenium",
                        data: "HTML didnt load properly"
                    });
                }
                console.log(" Ref type ID is : ", refTypeID);
                this.pause(1000)
                    .selectByValue(refTypeID, settings.refMap[refType])
                    .pause(8000)
                    .getHTML("/html/body")
                    .then(function(html) {
                        refID = htmlParser.refIDParser(html);
                        console.log("Ref ID : ", refTypeID);
                        console.log("dateOfBirthID : ", dateOfBirthID);
                        console.log("passportID : ", passportID);
                        console.log("countryID : ", countryID);
                        console.log("checkboxID : ", checkboxID);
                        console.log("submitID : ", submitID);

                        this.pause(5000)
                            .setValue(refID, refNumber)
                            .pause(500)
                            .setValue(dateOfBirthID, DOB)
                            .pause(500)
                            .setValue(passportID, passportNo)
                            .pause(500)
                            .selectByValue(countryID, settings.countries[country.toLowerCase()])
                            .pause(500)
                            .click(checkboxID)
                            .pause(500)
                            .click(submitID)
                            .pause(5000)
                            .getHTML("/html/body")
                            .then(function(html) {
                                console.log("Sending Data to Parse");
                                responseData = htmlParser.resultParser(html);
                                console.log("Got Parser Data");
                                console.log("Total Process took : %s seconds", (new Date() - startTime) / 1000);
                                this.end();
                                return cb(responseData);
                            });

                    });

            });
    };
    // myVevo("TRN", "EGOE2YJGFS", "08 Apr 1991", "J8044027", "India", function(response) {
    //     console.log("Final Response : ", response);
    // });
    // 
    
}());

// {
//     "india": "IND",
//     "pakistan": "PAK",
//     "bangladesh": "BGD",
//     "sri lanka": "LKA",
//     "philippines": "PHL",
// }