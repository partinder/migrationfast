(function() {
    const mongoClient = require("mongodb").MongoClient;
    const settings = require("../config/settings")();
    const db_path = "mongodb://" + settings.db_server + "/" + settings.db;

    var _db;

    exports = module.exports = {
        connect: function(cb) {
            mongoClient.connect(db_path, function(err, db) {
                console.log("Connected to DB");
                _db = db;
                return cb(err,db);
            });
        },
        get: function(){
        	return _db;
        }
    };
}());