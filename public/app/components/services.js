function botDataService($http, $location) {
    this.getOptionsList = function(optionsType) {
        console.log("Called Bot Data Options")
        var promise = new Promise(function(resolve, reject) {

            if (optionsType === "occupation") {
                var options = {
                    method: "GET",
                    url: "/api/v1/options_list",
                    params: {
                        options_type: optionsType
                    }
                }
                console.log(options)
                $http(options).then(function(response) {
                    // Sucess CallBack
                    if (!response.data.err) {
                        return resolve({
                            type: optionsType,
                            list: response.data.list
                        })
                    } else {
                        console.log(response)
                        return reject(response.err)
                    }

                }, function(err) {
                    //Failure Callback
                    reject(err)
                })
            }
            if (optionsType === "189_others") {
                return resolve({
                    type: optionsType,
                    list: [{
                        name: "Accredited in a community language",
                        option_id: "accredited"
                    }, {
                        name: "Study in regional Australia or a low population growth metropolitan area that meets the Australian study requirement",
                        option_id: "regional-area"
                    }, {
                        name: "Partner skill qualifications",
                        option_id: "partner",
                    }, {
                        name: "Professional year in Australia",
                        option_id: "professional-year"
                    }]
                })
            }

        })
        return promise;
    }

    this.postSelected = function(psid, data) {
        JL().info("Sending Selcted data")
        var options = {
            method: "POST",
            url: "/3ziye5jykl0bsqgfi1yb", // Same path as the path used for FB Messenger post actions
            data: {
                object: "webview",
                event: {
                    sender: {
                        id: psid
                    },
                    webview: {
                        payload: data
                    }
                }
            }
        }
        $http(options).then(function(response) {
            if (!response.data.err) {
                JL().info("Got post response wiht no errors")
                MessengerExtensions.requestCloseBrowser(function success() {
                    JL().info("Closed Web view")
                    console.log("Window Closed")
                }, function error(err) {
                    JL().info("cant close websview")
                    console.log("Error in closing WebView")
                });
            } else {
                console.log("Error from Server")
                $location.path('/404')
            }
        }, function(err) {
            console.log("Some Error from")
        })
    }
}
botDataService.$inject = ['$http', '$location']