(function() {
    // Process answers such has 189 Assessment and others
    var helpers = require("./helpers");
    var moment = require("moment");
    var Fuse = require("fuse.js");
    var myVevoManager = require("../../utils/myVevoManager");

    var check = {
        gsm_assessment: function(answers) {
            var score = {
                total: 0,
                breakUp: {}
            };
            var replyText;
            prepareScore();
            prepareAnswer();

            function prepareScore() {
                for (var key in answers) {
                    var add = 0;
                    switch (key) {
                        case 'occupation':
                            // Can you further diligence on Occupation/ CSOL/ SOL

                            break;
                        case 'user_age':
                            var age = Number(moment().diff(moment(answers['user_age'], 'DD/MM/YYYY'), 'years'));
                            if (age >= 18 && age <= 24) {
                                add += 25;
                            }
                            if (age >= 25 && age <= 32) {
                                add += 30;
                            }
                            if (age >= 33 && age <= 39) {
                                add += 25;
                            }
                            if (age >= 40 && age <= 44) {
                                add += 15;
                            }
                            score.total += add;
                            score.breakUp[key] = add;
                            break;
                        case 'maritial_status':
                            break;
                        case "skilled_employment_onshore":
                            if (answers[key] === 'yes') {
                                var years = Number(answers['employment_years']);
                                if (years >= 1 && years <= 2) {
                                    add += 5;
                                }
                                if (years >= 3 && years <= 4) {
                                    add += 10;
                                }
                                if (years >= 5 && years <= 7) {
                                    add += 15;
                                }
                                if (years >= 8 && years <= 10) {
                                    add += 20;
                                }
                            }
                            score.total += add;
                            score.breakUp[key] = add;
                            break;
                        case "skilled_employment_offshore":
                            var years = Number(answers[key]);
                            if (years >= 3 && years <= 4) {
                                add += 5;
                            }
                            if (years >= 5 && years <= 7) {
                                add += 10;
                            }
                            if (years >= 8 && years <= 10) {
                                add += 15;
                            }
                            score.total += add;
                            score.breakUp[key] = add;
                            break;
                        case "english":
                            if (answers[key] === 'yes') {
                                var r = Number(answers['english_score_reading']);
                                var l = Number(answers['english_score_listening']);
                                var w = Number(answers['english_score_writing']);
                                var s = Number(answers['english_score_speaking']);

                                switch (answers['which_english']) {
                                    case 'ielts':
                                        if ((r >= 7 && r < 8) && (l >= 7 && l < 8) && (w >= 7 && w < 8) && (s >= 7 && s < 8)) {
                                            add += 10;

                                        }
                                        if ((r >= 8) && (l >= 8) && (w >= 8) && (s >= 8)) {
                                            add += 20;
                                        }
                                        break;
                                    case 'pte':
                                        if ((r >= 65 && r < 79) && (l >= 65 && l < 79) && (w >= 65 && w < 79) && (s >= 65 && s < 79)) {
                                            add += 10;
                                        }
                                        if ((r >= 79) && (l >= 79) && (w >= 79) && (s >= 79)) {
                                            add += 20;
                                        }
                                        break;
                                }
                            }
                            score.total += add;
                            score.breakUp[key] = add;
                            break;
                        case 'qualification':
                            switch (answers[key]) {
                                case 'doctorate':
                                    add += 20;
                                    break;
                                case 'bachelor':
                                    add += 15;
                                    break;
                                case 'diploma-trade':
                                case 'others':
                                    add += 10;
                                    break;
                            }
                            score.total += add;
                            score.breakUp[key] = add;
                            break;
                        case "studied_in_australia":
                            if (answers[key] != 'no') {
                                add += 5;
                            }

                            score.total += add;
                            score.breakUp[key] = add;
                            break;
                        case "gsm_others":
                            if (answers[key] != 'no') {
                                add += 5;
                            }
                            score.total += add;
                            score.breakUp[key] = add;
                            break;
                    }
                }
                console.log(score);
            }

            function prepareAnswer() {

                var better = [];
                // Test English
                if (score.breakUp['english'] != 20) {
                    better.push('english');
                }
                if (answers['maritial_status'] == "married" && answers['gsm_others']['option_id'] != 'partner') {
                    // client is Married and might get more points if partner skills assessment
                    better.push('partner');
                }
                console.log(better);
                if (score.total < 60) {

                    replyText = "Ooops! Unfortunatly you curerntly do not qualify for a General Skilled Migration program. But you have a few options :\n" + "\n• Apply for a State Sponsored Visa\n";
                    if (better.indexOf('english') != -1) {
                        replyText = replyText.concat("\n• Improve your English testing score\n");
                    }
                    if (better.indexOf('partner') != -1) {
                        replyText = replyText.concat("\n• Get your partner's skills assessed\n");
                    }
                }
                if (score.total >= 60 && score.total <= 70) {
                    console.log("Opps");
                    replyText = "CONGRATULATIONS. You qualify for the GSM Visa program, but given the invitation waiting times for your occupation, you can strenthen your case further :\n" + "\n• Apply for a State Nomination.\n";
                    if (better.indexOf('english') != -1) {
                        console.log("Concat");
                        replyText = replyText.concat("\n• Improve your English score.\n");
                    }
                    if (better.indexOf('partner') != -1) {
                        replyText = replyText.concat("\n• Get your partner's skills assessed to score extra points.\n");
                    }
                }
                if (score.total > 70) {
                    replyText = "CONGRATULATIONS. You qualify for the GSM Visa program. You are among the top 95% of the applicants, your application would be processed on priority basis.";
                }
            }
            // Process Age
            return helpers.textMessage(replyText);
        },
        occupation: function(answer) {
            console.log(answer);
            var skills_list = require("../../config/data").skills_list;
            var options = {
                shouldSort: true,
                includeScore: true,
                threshold: 0.0,
                location: 0,
                distance: 100,
                maxPatternLength: 32,
                minMatchCharLength: 3,
                keys: [
                    "anzsco"
                ]
            };

            var replyText = "Ooops, Unfortuantely your selected occupation is not eligible for any visas.";
            var fuse = new Fuse(skills_list, options);
            var results = fuse.search(answer.occupation);
            console.log("ANSWERS result : ", results);
            if (results[0].item.visa.length > 0) {
                replyText = "CONGRATULATIONS, your occupation is eligible for the following visa(s) : ";
                results[0].item.visa.forEach(function(visaCode) {
                    replyText = replyText.concat("\n✓ " + visaCode);
                });
            }

            return helpers.textMessage(replyText);
        },
        my_visa_status: function(answer) {
            console.log("Creating new My Vevo request");
            answer.request_by = "fb";
            myVevoManager.request(answer);
            return helpers.textMessage("Thank you, we'll fetch your visa details and get back to you in a few minutes");
        }
    };
    module.exports = check;
    var questionManager = require("./questionManager");

    // var answers = {
    //     occupation: {
    //         name: 'Analyst Programmer',
    //         anzsco: '261311',
    //         assesing_authority: 'ACS'
    //     },
    //     user_age: '16/12/1983',
    //     maritial_status: 'never-married',
    //     skilled_employment_onshore: 'no',
    //     employment_years: '5',
    //     skilled_employment_offshore: '5',
    //     english: 'yes',
    //     which_english: 'ielts',
    //     english_score_listening: '7',
    //     english_score_speaking: '7',
    //     english_score_writing: '7',
    //     english_score_reading: '7',
    //     qualification: 'bachelor',
    //     studied_in_australia: 'no',
    //     qualification_australia: 'doctorate',
    //     specialist_education: 'no',
    //     'gsm_others': { name: 'Partner skill qualifications', option_id: 'partner' }
    // }

    // console.log(check.gsm_assessment(answers))

}());

// { occupation: 
//    { name: 'Analyst Programmer',
//      anzsco: '261311',
//      assesing_authority: 'ACS' },
//   user_age: '11/2/1983',
//   maritial_status: 'never-married',
//   skilled_employment_onshore: 'yes',
//   employment_years: '5',
//   skilled_employment_offshore: '6',
//   english: 'yes',
//   which_english: 'ielts',
//   english_score_listening: '7',
//   english_score_speaking: '7',
//   english_score_writing: '7',
//   english_score_reading: '7',
//   qualification: 'bachelor',
//   studied_in_australia: 'yes',
//   qualification_australia: 'doctorate',
//   specialist_education: 'no',
//   '189_others': { name: 'Partner skill qualifications', option_id: 'partner' } }