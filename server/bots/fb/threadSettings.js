(function() {
    // We define and setup static Theread Settings like Getting Started Button, Welcome text etc.
    const request = require('request');
    const settings = require('../../config/settings')();
    var threadSettings = {};
    // Setup Get Started Button
    threadSettings.setupGetStarted = function() {
        var options = {
            method: 'POST',
            uri: 'https://graph.facebook.com/v2.6/me/thread_settings',
            qs: {
                access_token: settings.fb.page_access_token
            },
            json: {
                setting_type: 'call_to_actions',
                thread_state: 'new_thread',
                call_to_actions: [{
                    payload: "get_started_button"
                }]
            }
        };

        request(options, function(err, res, body) {
            if (!err && res.statusCode == 200) {
                console.log(body);
            } else {
                console.log(err);
                console.log(res.body);
            }
        });
    };
    threadSettings.setupGreetingText = function() {
        var options = {
            method: 'POST',
            uri: 'https://graph.facebook.com/v2.6/me/thread_settings',
            qs: {
                access_token: settings.fb.page_access_token
            },
            json: {
                setting_type: 'greeting',
                greeting: {
                    text: 'Hi {{user_full_name}}, I am a bot made my KP Singh to help you better asses your eligiblity for different visas.'
                }
            }
        };

        request(options, function(err, res, body) {
            if (!err && res.statusCode == 200) {
                console.log(body);
            } else {
                console.log(err);
                console.log(res.body);
            }
        });
    };
    threadSettings.setupPersistentMenu = function() {
        var options = {
            method: 'POST',
            uri: 'https://graph.facebook.com/v2.6/me/messenger_profile',
            qs: {
                access_token: settings.fb.page_access_token
            },
            json: {
                persistent_menu: [{
                    locale: 'default',
                    composer_input_disabled: false,
                    call_to_actions: [{
                        title: 'Ask a Question',
                        type: 'postback',
                        payload: 'query_intro'
                    }, {
                        title: "Assess Occupation",
                        type: "postback",
                        payload: "my_occupation"
                    }, {
                        title: "My Visa Status",
                        type: "postback",
                        payload: "my_visa_status"
                    }]
                }]
            }
        };


        request(options, function(err, res, body) {
            if (!err && res.statusCode == 200) {
                console.log(body);
            } else {
                console.log(err);
                console.log(res.body);
            }
        });
    };
    threadSettings.whitelistDomain = function() {
        var options = {
            method: 'POST',
            uri: 'https://graph.facebook.com/v2.6/me/messenger_profile',
            qs: {
                access_token: settings.fb.page_access_token
            },
            json: {
                whitelisted_domains: [
                    settings.server_url
                ]
            }
        };

        request(options, function(err, res, body) {
            if (!err && res.statusCode == 200) {
                console.log(body);
            } else {
                console.log(err);
                console.log(res.body);
            }
        });
    };

    threadSettings.domains = function() {
        var options = {
            method: "GET",
            uri: 'https://graph.facebook.com/v2.6/me/thread_settings?fields=whitelisted_domains',
            qs: {
                access_token: settings.fb.page_access_token
            }
        };

        request(options, function(err, res, body) {
            console.log(body);
        });
    };

    if (!process.argv[2]) {
        Object.keys(threadSettings).forEach(function(name) {
            threadSettings[name]();
        });
    } else {
        threadSettings[process.argv[2]]();
    }

})();